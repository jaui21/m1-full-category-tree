<?php
require_once('app/Mage.php');
Mage::app(); 

echo "<pre>";
$category = Mage::getModel('catalog/category');
$tree = $category->getTreeModel();
$tree->load();
 
$ids = $tree->getCollection()->getAllIds();
$categories = array();
if ($ids)
{
    foreach ($ids as $id)
    {
        $category->load($id);
        $categories[$id]['name'] = $category->getName();
        $categories[$id]['path'] = $category->getPath();
    }
    foreach ($ids as $id)
    {
        $path = explode('/', $categories[$id]['path']);
        $string = '';
        foreach ($path as $pathId)
        {
            $string.= $categories[$pathId]['name'] . ' > ';
            $cnt++;
        }
        $string.= ';' . $id . "\n";
 
        echo $string;
    }
}

?>